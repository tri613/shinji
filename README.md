# shinji

kaworu kun daisuki

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/shinji.svg)](https://npmjs.org/package/shinji)
[![Downloads/week](https://img.shields.io/npm/dw/shinji.svg)](https://npmjs.org/package/shinji)
[![License](https://img.shields.io/npm/l/shinji.svg)](https://github.com/tri613/shinji/blob/master/package.json)

<!-- toc -->
* [shinji](#shinji)
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->

# Usage

<!-- usage -->
```sh-session
$ npm install -g shinji
$ shinji COMMAND
running command...
$ shinji (-v|--version|version)
shinji/1.1.0 darwin-x64 node-v12.13.1
$ shinji --help [COMMAND]
USAGE
  $ shinji COMMAND
...
```
<!-- usagestop -->

# Commands

<!-- commands -->
* [`shinji clean`](#shinji-clean)
* [`shinji help [COMMAND]`](#shinji-help-command)

## `shinji clean`

clean up your stale branches 💖

```
USAGE
  $ shinji clean

OPTIONS
  -f, --force  force delete
  -h, --help   show CLI help

EXAMPLE
  $ shinji clean
```

_See code: [src/commands/clean.ts](https://github.com/tri613/shinji/blob/v1.1.0/src/commands/clean.ts)_

## `shinji help [COMMAND]`

display help for shinji

```
USAGE
  $ shinji help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v2.2.3/src/commands/help.ts)_
<!-- commandsstop -->
