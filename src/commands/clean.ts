import { Command, flags } from "@oclif/command";
import * as simpleGit from "simple-git/promise";
import * as inquirer from "inquirer";
import * as chalk from "chalk";

const git = simpleGit(process.cwd());

export default class Clean extends Command {
  static description = "clean up your stale branches 💖";

  static examples = [`$ shinji clean`];

  static flags = {
    help: flags.help({ char: "h" }),
    force: flags.boolean({ char: "f", description: "force delete" }),
    "skip-remote": flags.boolean({
      description: "skip pulling from remote",
    }),
  };

  branches = new Map();
  remotes: string[] = [];

  async run() {
    try {
      const { flags } = this.parse(Clean);

      await this.setRemotes();
      if (!flags["skip-remote"]) {
        await this.fetchRemotes();
      }

      await this.setBranches();

      const choices = [...this.branches.keys()]
        .filter((b) => !["master", "develop"].includes(b))
        .map((b) => this.mapBranchToChoices(b));

      if (!choices.length) {
        this.log(
          chalk.yellowBright(
            "No branch to delete! (`master` & `develop` branches are excluded)"
          )
        );
        return;
      }

      const selected = await this.selectBranches(choices);

      if (!selected.length) {
        this.log(
          chalk.red(
            "You didn't select any branches! I'll just do nothing then!"
          )
        );
        return;
      }

      try {
        await this.deleteBranches(selected, flags.force);
      } catch (err) {
        this.log(chalk.red(`Oops, looks like some branches can't be deleted.`));

        // fetch local branches again
        this.setBranches();
        const failedBranches = selected.filter((choice) =>
          this.branches.has(choice)
        );

        const selectedFailed = await this.selectBranches(
          failedBranches.map((b) => this.mapBranchToChoices(b)),
          "Do you want to try force deleting those branches?"
        );

        await this.deleteBranches(selectedFailed, true);
      }

      this.log(chalk.blue("Clean up done! 💓"));
    } catch (err) {
      this.log(chalk.red("Something went wrong 🥺"));
      this.error(err);
    }
  }

  setRemotes() {
    return git
      .getRemotes()
      .then((remotes) => (this.remotes = remotes.map((r) => r.name)));
  }

  fetchRemotes() {
    this.log(chalk.gray("Fetching remote first..."));

    if (!this.remotes.length) {
      return git.fetch();
    }

    const promises = this.remotes.map((remote) =>
      git.fetch([remote, "--prune"])
    );
    return Promise.all(promises);
  }

  async setBranches() {
    const localBranches = await git.branch(["-vv"]);
    Object.values(localBranches.branches).forEach((s) => {
      this.branches.set(s.name, s.label);
    });
  }

  mapBranchToChoices(branch: string) {
    return {
      name: `${chalk.yellow.bold(branch)} ${this.branches.get(branch)}`,
      value: branch,
    };
  }

  async selectBranches(
    choices: any[],
    message = "Select the branches you want to delete."
  ) {
    const questions = [
      {
        type: "checkbox",
        message,
        name: "branches",
        choices,
        pageSize: 20,
      },
    ];

    const { branches: selected } = (await inquirer.prompt(questions)) as {
      branches: string[];
    };

    return selected;
  }

  async deleteBranches(selected: string[], force: boolean) {
    const response = (await (git.deleteLocalBranches(
      selected,
      force
    ) as unknown)) as {
      errors: any[];
      all: any[];
    };

    const deleted = response.all
      .filter((row) => row.success)
      .map((row) => row.branch);

    if (deleted.length) {
      this.log(
        `These branches have been removed: ${chalk.green.bold(
          deleted.join(", ")
        )}`
      );
    }

    return response;
  }
}
